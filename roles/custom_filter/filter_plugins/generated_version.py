import datetime

def version (**kwargs):
    inv = kwargs.get('inv', None)
    date = datetime.datetime.utcnow().strftime('%Y-%m-%d')
    if inv is not None:
        return "{0}.{1}".format(inv, date)
    else:
        return date

class FilterModule(object):
    def filters(self):
        return {
            'get_version': lambda base, **kwargs: base + version(**kwargs)
        }
