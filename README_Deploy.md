# Description

**simple Ansible console tools for create/update AWS API gateway

## Installation

Must have ansible installed.

```console
pip install -r requirements.txt
```

## Overview

simple start command for create/update:

```
ansible-playbook -l test deploy-api-gateway.yml

```

clear prevision installation:

```
ansible-playbook -l test set-apiid-none-api-gateway.yml

```

## Configuration

./inventory/apilists - group list with hosts
./inventory/group_vars - set group variables
./inventory/host_vars/dev (staging, prod .....) - set host variables
