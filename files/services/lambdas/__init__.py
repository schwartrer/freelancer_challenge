from __future__ import print_function
"""
About handler returns information information about this api
"""

def my_handler(event, context):

    return {
        'message': "This is the about message (event=%s)" % type(event),
        'version': context.function_version,
        'request': context.aws_request_id
    }
