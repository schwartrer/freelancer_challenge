from __future__ import print_function

def about_handler(event, context):
    """
    Returns some very simple data about the api
    :param event:
    :param context:
    :return:
    """
    print("Handling about request")
    return {
        "message": "This is the about endpoint for %s (event=%s)" % (context.client_context,
                                                                     type(event)),
        "version": context.function_version,
    }